from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    phone = db.Column(db.String(12), unique=True)

    def __init__(self, name, phone):
        self.name = name
        self.phone = phone

    def __html__(self):
     return repr(self)


class ContactSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('name', 'phone')


contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)


# endpoint to create new user
@app.route("/contact", methods=["POST"])
def add_contact():
    name = request.json['name']
    phone = request.json['phone']
    
    new_contact = Contact(name, phone)

    db.session.add(new_contact)
    db.session.commit()

    return jsonify(new_contact)


# endpoint to show all users
@app.route("/contact", methods=["GET"])
def get_contact():
    all_contact = Contact.query.all()
    result = contacts_schema.dump(all_contact)
    return jsonify(result.data)


# endpoint to get user detail by id
@app.route("/contact/<id>", methods=["GET"])
def contact_detail(id):
    contact = Contact.query.get(id)
    return contact_schema.jsonify(contact)


# endpoint to update user
@app.route("/contact/<id>", methods=["PUT"])
def contact_update(id):
    contact = Contact.query.get(id)
    name = request.json['name']
    phone = request.json['phone']

    contact.name = name
    contact.phone = phone

    db.session.commit()
    return contact_schema.jsonify(contact)


# endpoint to delete user
@app.route("/contact/<id>", methods=["DELETE"])
def contact_delete(id):
    contact = Contact.query.get(id)
    db.session.delete(contact)
    db.session.commit()

    return contact_schema.jsonify(contact)


if __name__ == '__main__':
    app.run(debug=True)
